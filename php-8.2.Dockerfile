FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    software-properties-common \
    && add-apt-repository ppa:ondrej/php \
    && apt-get update && apt-get install -y \
        curl \
        build-essential \
        ruby \
        ruby-dev \
        docker-compose \
        mysql-client \
        php8.2-fpm \
        php8.2-bcmath \
        php8.2-cli \
        php8.2-curl \
        php8.2-gd \
        php8.2-imagick \
        php8.2-mbstring \
        php8.2-mongodb \
        php8.2-xml \
        php8.2-zip \
        dh-autoreconf \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install -y \
        nodejs \
        yarn \
    && rm -rf /var/lib/apt/lists/*

# Copy and run the Composer installation script
COPY install_composer.sh /usr/local/bin/install_composer.sh
RUN chmod +x /usr/local/bin/install_composer.sh
RUN /usr/local/bin/install_composer.sh

# Install packages
RUN gem install scss_lint

# Get Lint config
RUN curl -O https://bitbucket.org/garethhallnz/scss-lint-config/raw/31fc5717bc1c24dac1d6ef4e3a9503d7a67aecf7/.scss-lint.yml
RUN mv .scss-lint.yml ~/

# Copy and run the Lagoon CLI installation script
COPY install_lagoon.sh /usr/local/bin/install_lagoon.sh
RUN chmod +x /usr/local/bin/install_lagoon.sh
RUN /usr/local/bin/install_lagoon.sh

# Install Sentry CLI
RUN curl -sL https://sentry.io/get-cli/ | bash
