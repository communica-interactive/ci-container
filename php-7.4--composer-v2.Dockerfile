FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    software-properties-common \
    curl \
    build-essential \
    ruby \
    ruby-dev \
    docker-compose \
    mysql-client \
    dh-autoreconf

RUN add-apt-repository ppa:ondrej/php \
    && apt-get update && apt-get install -y \
        php7.4-fpm \
        php7.4-bcmath \
        php7.4-cli \
        php7.4-curl \
        php7.4-gd \
        php7.4-mbstring \
        php7.4-mongodb \
        php7.4-xml \
        php7.4-zip

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install -y \
        nodejs \
        yarn

RUN rm -rf /var/lib/apt/lists/*

# Copy and run the Composer installation script
COPY install_composer.sh /usr/local/bin/install_composer.sh
RUN chmod +x /usr/local/bin/install_composer.sh
RUN /usr/local/bin/install_composer.sh

# Install packages
RUN gem install scss_lint

# Get Lint config
RUN curl -O https://bitbucket.org/garethhallnz/scss-lint-config/raw/31fc5717bc1c24dac1d6ef4e3a9503d7a67aecf7/.scss-lint.yml
RUN mv .scss-lint.yml ~/

# Copy and run the Lagoon CLI installation script
COPY install_lagoon.sh /usr/local/bin/install_lagoon.sh
RUN chmod +x /usr/local/bin/install_lagoon.sh
RUN /usr/local/bin/install_lagoon.sh

# Install Sentry CLI
RUN curl -sL https://sentry.io/get-cli/ | bash
