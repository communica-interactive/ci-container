# Building the docker Images

To build the Docker images for different PHP versions, use the following commands:

```bash
# Build PHP 8.3 image
docker build --pull --rm -f php-8.3.Dockerfile -t communica/ci-container:php-8.3 .

# Build PHP 8.2 image
docker build --pull --rm -f php-8.2.Dockerfile -t communica/ci-container:php-8.2 .

# Build PHP 8.1 image
docker build --pull --rm -f php-8.1.Dockerfile -t communica/ci-container:php-8.1 .

# Build PHP 8.0 image
docker build --pull --rm -f php-8.0.Dockerfile -t communica/ci-container:php-8.0 .

# Build PHP 7.4 composer 2 image
docker build --pull --rm -f php-7.4--composer-v2.Dockerfile -t communica/ci-container:php-7.4--composer-v2 .
```

# Push it to hub.docker.com

Before pushing the Docker images to Docker Hub, make sure you are logged in. Use the following command to log in:
```bash
# Login to hub.docker.com
docker login


# Push PHP 8.3 image
docker image push communica/ci-container:php-8.3

# Push PHP 8.2 image
docker image push communica/ci-container:php-8.2

# Push PHP 8.1 image
docker image push communica/ci-container:php-8.1

# Push PHP 8.0 image
docker image push communica/ci-container:php-8.0

# Push PHP 7.4 composer 2 image
docker image push communica/ci-container:php-7.4--composer-v2
```

# Running the docker containers locally
```bash
# Run PHP 8.3 container
docker run --rm -it communica/ci-container:php-8.3

# Run PHP 8.2 container
docker run --rm -it communica/ci-container:php-8.2

# Run PHP 8.1 container
docker run --rm -it communica/ci-container:php-8.1

# Run PHP 8.0 container
docker run --rm -it communica/ci-container:php-8.0

# Run PHP 7.4 composer 2 image
docker run --rm -it communica/ci-container:php-7.4--composer-v2
```