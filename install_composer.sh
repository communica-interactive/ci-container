#!/bin/bash

# Download and install Composer
curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin --2

# Set environment variable for Composer
export COMPOSER_ALLOW_SUPERUSER=1

# Export the Composer bin directory to PATH
echo 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> /etc/profile.d/composer.sh
