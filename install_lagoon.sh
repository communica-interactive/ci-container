#!/bin/bash

# Download and install Lagoon CLI
curl -L "https://github.com/uselagoon/lagoon-cli/releases/download/v0.21.3/lagoon-cli-v0.21.3-linux-amd64" -o /usr/local/bin/lagoon
chmod +x /usr/local/bin/lagoon
