FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    software-properties-common \
    && add-apt-repository ppa:ondrej/php \
    && apt-get update && apt-get install -y \
        curl \
        build-essential \
        docker-compose \
        mysql-client \
        php8.3-fpm \
        php8.3-bcmath \
        php8.3-cli \
        php8.3-curl \
        php8.3-gd \
        php8.3-imagick \
        php8.3-mbstring \
        php8.3-mongodb \
        php8.3-xml \
        php8.3-zip \
        dh-autoreconf \
    && curl -sL https://deb.nodesource.com/setup_18.x | bash - \
    && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install -y \
        nodejs \
        yarn \
    && rm -rf /var/lib/apt/lists/*

# Copy and run the Composer installation script
COPY install_composer.sh /usr/local/bin/install_composer.sh
RUN chmod +x /usr/local/bin/install_composer.sh
RUN /usr/local/bin/install_composer.sh

# Copy and run the Lagoon CLI installation script
COPY install_lagoon.sh /usr/local/bin/install_lagoon.sh
RUN chmod +x /usr/local/bin/install_lagoon.sh
RUN /usr/local/bin/install_lagoon.sh

# Install Sentry CLI
RUN curl -sL https://sentry.io/get-cli/ | bash
